# Zoovu Game Backend

A backend application to support the “Make Our Logo Great Again” game.

It handles the authentication layer using a Json Web Tokens and exposes an API to get leaderboard table results.

## ❕Before Running App

In order to sign Json Web Tokens the app is using a JKS file. Please execute the following command to generate the file:

```
keytool -genkey -alias zoovu-game -keyalg RSA -keystore zoovu-game.jks -keysize 2048
```

After the generation of the JKS file, please copy it into the project resources folder.
When this is done, the app should be able to generate JWTs with no problems.

## ℹ️ Features

### Authentication Layer

In order to add security to the game backend, the app requires authentication.

There are endpoints to manage the sign up, login, verify account and logout process.

### Methods to Play Game

The Game controller provides functionality to play the game. 
It has methods to create a game, make a move and end the game.

Additionally, there are mock methods that explain how to enable co-operative mode and reset the game results.

### Leaderboard Results
The Leaderboard controller exposes information regarding the best players of today, this week, this year and all time.

## 👨🏻‍💻 API

- Please make sure to include the access token in all protected requests as a Bearer token.

- You can review all API methods available by visiting the following URL (no login required):
    
    ```
    http://localhost:8080/swagger-ui.html
    ``` 

- There is also a Postman collection in the root directory, this json can be imported in Postman to check the endpoints and responses.

## Testing

The app has test methods to verify the behaviour of the Leaderboard service.