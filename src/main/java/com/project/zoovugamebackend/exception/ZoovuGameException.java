package com.project.zoovugamebackend.exception;

public class ZoovuGameException extends RuntimeException {
    public ZoovuGameException(String message) {
        super(message);
    }
}
