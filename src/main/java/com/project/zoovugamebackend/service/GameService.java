package com.project.zoovugamebackend.service;

import com.project.zoovugamebackend.dto.GameRequest;
import com.project.zoovugamebackend.dto.GameResponse;
import com.project.zoovugamebackend.dto.GameTurnRequest;
import com.project.zoovugamebackend.dto.GameTurnResponse;
import com.project.zoovugamebackend.exception.ZoovuGameException;
import com.project.zoovugamebackend.model.Game;
import com.project.zoovugamebackend.model.GameStatus;
import com.project.zoovugamebackend.model.GameTurn;
import com.project.zoovugamebackend.model.User;
import com.project.zoovugamebackend.repository.GameRepository;
import com.project.zoovugamebackend.repository.GameTurnsRepository;
import com.project.zoovugamebackend.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class GameService {

    private final AuthService authService;
    private final GameRepository gameRepository;
    private final UserRepository userRepository;
    private final GameTurnsRepository gameTurnsRepository;

    public GameResponse create() {
        // Create game
        Game game = new Game();
        game.setUuid(UUID.randomUUID().toString());
        game.setCreatedDate(Instant.now());
        game.setGameStatus(GameStatus.STARTED);

        game = gameRepository.save(game);

        User user = authService.getCurrentUser();

        // Create Game Turn
        GameTurn gameTurn = new GameTurn();
        gameTurn.setUser(user);
        gameTurn.setGame(game);
        gameTurn.setUserTurn(true);

        gameTurnsRepository.save(gameTurn);

        return GameResponse.builder()
                .gameId(game.getId())
                .gameUuid(game.getUuid())
                .createdDate(game.getCreatedDate())
                .build();
    }

    public GameTurnResponse move(GameRequest gameRequest) {
        // Find User
        User user = authService.getCurrentUser();

        // Find Game
        String gameUuid = gameRequest.getGameUuid();
        Game game = gameRepository.findByUuid(gameUuid).orElseThrow(
                () -> new ZoovuGameException(("Game was not found: " + gameUuid))
        );

        // Update Game Turn
        GameTurn gameTurn = gameTurnsRepository.findByUserAndGame(user, game).orElseThrow(
                () -> new ZoovuGameException("Game Turn was not found for the User: " + user.getUsername() + " in Game: " + gameUuid)
        );

        gameTurn.setLastPlayedAt(Instant.now());
        gameTurnsRepository.save(gameTurn);

        return GameTurnResponse.builder()
                .lastPlayedAt(gameTurn.getLastPlayedAt())
                .userTurn(gameTurn.getUserTurn())
                .build();
    }

    public void end(GameRequest gameRequest) {
        // Find Game
        String gameUuid = gameRequest.getGameUuid();
        Game game = gameRepository.findByUuid(gameUuid).orElseThrow(
                () -> new ZoovuGameException("Game was not found: " + gameUuid)
        );

        // Check user is able to end game
        List<GameTurn> gameTurns = gameTurnsRepository.findAllByGame(game).orElseThrow(
                () -> new ZoovuGameException("No existing turns for game: " + gameUuid)
        );

        User user = authService.getCurrentUser();
        String username = user.getUsername();

        gameTurns = gameTurns
                .stream()
                .filter(gt -> gt.getUser().getUsername().equals(username))
                .collect(Collectors.toList());

        if (gameTurns.size() == 0) {
            throw new ZoovuGameException("User is not allowed to end game: " + username);
        }

        // CHeck game has not finished yet
        if (game.getGameStatus().equals(GameStatus.FINISHED)) {
            throw new ZoovuGameException("Game has already finished");
        }

        game.setGameStatus(GameStatus.FINISHED);
        game.setResultInSec(gameRequest.getResultInSec());
        gameRepository.save(game);
    }
}

