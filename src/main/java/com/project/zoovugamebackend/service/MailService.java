package com.project.zoovugamebackend.service;

import com.project.zoovugamebackend.exception.ZoovuGameException;
import com.project.zoovugamebackend.model.EmailNotification;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@Slf4j
public class MailService {

    private final JavaMailSender mailSender;
    private final MailContentBuilder mailContentBuilder;

    public void sendMail(EmailNotification emailNotification) {
        MimeMessagePreparator mimeMessagePreparator = mimeMessage -> {
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);

            mimeMessageHelper.setFrom("zoovu-game@gmail.com");
            mimeMessageHelper.setTo(emailNotification.getRecipient());
            mimeMessageHelper.setSubject(emailNotification.getSubject());
            mimeMessageHelper.setText(mailContentBuilder.build(emailNotification.getBody()));
        };

        try {
            mailSender.send(mimeMessagePreparator);
            log.info("Activation email was sent!");
        } catch (MailException e) {
            throw new ZoovuGameException(
                    "Exception occurred when sending email to: " + emailNotification.getRecipient()
            );
        }
    }
}
