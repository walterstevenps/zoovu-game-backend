package com.project.zoovugamebackend.service;

import com.project.zoovugamebackend.dto.LeaderBoardResponse;
import com.project.zoovugamebackend.model.GameStatus;
import com.project.zoovugamebackend.model.GameTurn;
import com.project.zoovugamebackend.repository.GameTurnsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.threeten.extra.YearWeek;

import java.time.*;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class LeaderBoardService {

    @Autowired
    private GameTurnsRepository gameTurnsRepository;

    /**
     * Get Best Players All the Time
     */
    public List<LeaderBoardResponse> bestPlayers() {
        List<GameTurn> gameTurns = getFinishedGames();

        return buildLeaderBoardResponse(gameTurns);
    }

    /**
     * Get Best Players of Today
     */
    public List<LeaderBoardResponse> bestPlayersToday() {
        List<GameTurn> gameTurns = getFinishedGames();

        List<GameTurn> filteredGameTurns = gameTurns.stream()
                .filter(gameTurn -> gameTurn.getGame().getCreatedDateUTC().equals(LocalDate.ofInstant(Instant.now(), ZoneOffset.UTC)))
                .collect(Collectors.toList());

        return buildLeaderBoardResponse(filteredGameTurns);
    }

    /**
     * Get Best Players this Week
     */
    public List<LeaderBoardResponse> bestPlayersThisWeek() {
        List<GameTurn> gameTurns = getFinishedGames();

        List<GameTurn> filteredGameTurns = gameTurns.stream()
                .filter(gameTurn -> YearWeek.now().equals(YearWeek.from(gameTurn.getGame().getCreatedDateUTC())))
                .collect(Collectors.toList());

        return buildLeaderBoardResponse(filteredGameTurns);
    }

    /**
     * Get Best Players This Year
     */
    public List<LeaderBoardResponse> bestPlayersThisYear() {
        List<GameTurn> gameTurns = getFinishedGames();

        List<GameTurn> filteredGameTurns = gameTurns.stream()
                .filter(gameTurn -> Year.now().equals(Year.from(gameTurn.getGame().getCreatedDateUTC())))
                .collect(Collectors.toList());

        return buildLeaderBoardResponse(filteredGameTurns);
    }

    /**
     * Method to get Top 10 Finished Games
     * @return
     */
    private List<GameTurn> getFinishedGames() {
        return gameTurnsRepository.findTop10ByGameGameStatusOrderByGameResultInSecAsc(GameStatus.FINISHED);
    }

    /**
     * Method to build the Leaderboard Response list
     */
    private List<LeaderBoardResponse> buildLeaderBoardResponse(List<GameTurn> gameTurns) {
        return gameTurns.stream()
                .map(gameTurn -> {
                    return LeaderBoardResponse.builder()
                            .username(gameTurn.getUser().getUsername())
                            .resultInSec(gameTurn.getGame().getResultInSec())
                            .build();
                })
                .collect(Collectors.toList());
    }
}
