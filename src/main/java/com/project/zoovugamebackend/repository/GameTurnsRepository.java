package com.project.zoovugamebackend.repository;

import com.project.zoovugamebackend.model.Game;
import com.project.zoovugamebackend.model.GameStatus;
import com.project.zoovugamebackend.model.GameTurn;
import com.project.zoovugamebackend.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface GameTurnsRepository extends JpaRepository<GameTurn, Long> {
    Optional<GameTurn> findByUserAndGame(User user, Game game);

    Optional<List<GameTurn>> findAllByGame(Game game);

    List<GameTurn> findTop10ByOrderByGameResultInSecDesc();

    /**
     * Get Top 10 Games Ordered By the Game Result
     */
    List<GameTurn> findTop10ByGameGameStatusOrderByGameResultInSecAsc(GameStatus gameStatus);
}
