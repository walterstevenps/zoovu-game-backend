package com.project.zoovugamebackend;

import com.project.zoovugamebackend.config.SwaggerConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@Slf4j
@Import(SwaggerConfig.class)
public class ZoovuGameBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZoovuGameBackendApplication.class, args);
	}

}
