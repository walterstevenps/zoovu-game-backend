package com.project.zoovugamebackend.model;

import lombok.*;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Set;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Game {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String uuid;


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "game")
    private Set<GameTurn> gameTurns;

    private Integer resultInSec;

    private Instant createdDate;

    private GameStatus gameStatus;

    public LocalDate getCreatedDateUTC() {
        return LocalDate.ofInstant(this.getCreatedDate(), ZoneOffset.UTC);
    }
}
