package com.project.zoovugamebackend.model;

public enum GameStatus {
    WAITING,
    STARTED,
    FINISHED;
}
