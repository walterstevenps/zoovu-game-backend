package com.project.zoovugamebackend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GameTurnRequest {

    @NotBlank
    private String username;

    @NotBlank
    private String gameUuid;
}
