package com.project.zoovugamebackend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GameRequest {

    @NotBlank
    private String gameUuid;

    private Integer resultInSec;
}
