package com.project.zoovugamebackend.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GameResponse {

    private Long gameId;
    private String gameUuid;
    private Integer resultInSec;
    private Instant createdDate;
}
