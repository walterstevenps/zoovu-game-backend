package com.project.zoovugamebackend.controller;

import com.project.zoovugamebackend.dto.AuthenticationResponse;
import com.project.zoovugamebackend.dto.LoginRequest;
import com.project.zoovugamebackend.dto.RefreshTokenRequest;
import com.project.zoovugamebackend.dto.RegisterRequest;
import com.project.zoovugamebackend.service.AuthService;
import com.project.zoovugamebackend.service.RefreshTokenService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/auth")
@AllArgsConstructor
public class AuthController {

    private final AuthService authService;
    private final RefreshTokenService refreshTokenService;

    /**
     * Feature: Sign Up for Account
     * @param registerRequest
     * @return
     */
    @PostMapping("/signup")
    public ResponseEntity<String> signup(@RequestBody RegisterRequest registerRequest) {
        authService.signup(registerRequest);

        return new ResponseEntity<>("User was registered successfully, please verify your account.", HttpStatus.OK);
    }

    /**
     * Feature: Possibility to verify Account via an email.
     * @param token
     * @return
     */
    @GetMapping("/accountVerification/{token}")
    public ResponseEntity<String> verifyAccount(@PathVariable String token) {
        authService.verifyAccount(token);

        return new ResponseEntity<>("Account activated successfully.", HttpStatus.OK);
    }

    /**
     * Feature: Login to Account
     * @param loginRequest
     * @return
     */
    @PostMapping("/login")
    public AuthenticationResponse login(@RequestBody LoginRequest loginRequest) {
        return authService.login(loginRequest);
    }

    /**
     * Feature: Logout from Account
     * @param refreshTokenRequest
     * @return
     */
    @PostMapping("/logout")
    public ResponseEntity<String> logout(@Valid @RequestBody RefreshTokenRequest refreshTokenRequest) {
        refreshTokenService.deleteRefreshToken(refreshTokenRequest.getRefreshToken());

        return ResponseEntity.status(HttpStatus.OK).body("Refresh Token was deleted successfully.");
    }
}
