package com.project.zoovugamebackend.controller;

import com.project.zoovugamebackend.dto.LeaderBoardResponse;
import com.project.zoovugamebackend.service.LeaderBoardService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/leaderboard")
@AllArgsConstructor
public class LeaderBoardController {

    private final LeaderBoardService leaderBoardService;

    /**
     * Get Best Players of Today
     * @return
     */
    @GetMapping("/today")
    public ResponseEntity<List<LeaderBoardResponse>> bestPlayersToday() {
        List<LeaderBoardResponse> leaderBoardResponseList = leaderBoardService.bestPlayersToday();

        return new ResponseEntity<>(leaderBoardResponseList, HttpStatus.OK);
    }

    /**
     * Gest Best Players this Week
     * @return
     */
    @GetMapping("/week")
    public ResponseEntity<List<LeaderBoardResponse>> bestPlayersThisWeek() {
        List<LeaderBoardResponse> leaderBoardResponseList = leaderBoardService.bestPlayersThisWeek();

        return new ResponseEntity<>(leaderBoardResponseList, HttpStatus.OK);
    }

    /**
     * Get Best Players this Year
     * @return
     */
    @GetMapping("/year")
    public ResponseEntity<List<LeaderBoardResponse>> bestPlayersThisYear() {
        List<LeaderBoardResponse> leaderBoardResponseList = leaderBoardService.bestPlayersThisYear();

        return new ResponseEntity<>(leaderBoardResponseList, HttpStatus.OK);
    }

    /**
     * Get Best Players all Time
     * @return
     */
    @GetMapping("/all")
    public ResponseEntity<List<LeaderBoardResponse>> bestPlayers() {
        List<LeaderBoardResponse> leaderBoardResponseList = leaderBoardService.bestPlayers();

        return new ResponseEntity<>(leaderBoardResponseList, HttpStatus.OK);
    }
}
