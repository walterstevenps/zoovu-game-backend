package com.project.zoovugamebackend.controller;

import com.project.zoovugamebackend.dto.GameRequest;
import com.project.zoovugamebackend.dto.GameResponse;
import com.project.zoovugamebackend.dto.GameTurnRequest;
import com.project.zoovugamebackend.dto.GameTurnResponse;
import com.project.zoovugamebackend.model.GameTurn;
import com.project.zoovugamebackend.service.GameService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/game")
@AllArgsConstructor
public class GameController {

    private final GameService gameService;

    /**
     * Feature: reate a Game
     */
    @GetMapping("/create")
    public GameResponse create() {
        return gameService.create();
    }

    /**
     * Feature: Register last time the player made a move
     * @param gameRequest
     */
    @PostMapping("/move")
    public GameTurnResponse move(@Valid @RequestBody GameRequest gameRequest) {
        return gameService.move(gameRequest);
    }

    /**
     * Feature: End Game
     * @param gameRequest
     */
    @PostMapping("/end")
    public ResponseEntity<String> end(@Valid @RequestBody GameRequest gameRequest) {
        gameService.end(gameRequest);

        return new ResponseEntity<>("Game finished successfully.", HttpStatus.OK);
    }

    /**
     * Co-operative mode: Ability to Join an existing game
     */
    @PostMapping("/join")
    public ResponseEntity<String> join(@Valid @RequestBody GameRequest gameRequest) {
        /**
         *  Given an existing game
         *  When a new user use the Game UUID to join a game
         *  Then the application should allow it
         */

        /**
         * The Game entity already has a UUID, so it will be possible for someone to join a game.
         *
         * The following logic should be executed in a new service method:
         *
         * 1. Find the existing game based on the UUID.
         * 2. Create a new Game Turn related to the new user (find it based on the JWT) and the game found.
         * 3. Set the userTurn field in the GameTurn to false in order to temporarily disable user moves.
         * 4. Update the Move method so next time someone makes a move, it should check if this is allowed and
         *      update the GameTurn's last played at field.
         * 5. Additionally, the Move method should get all GameTurns,
         *      update the current user turn to false, and update the other user turn to true so the user can make a move.
         */

        return new ResponseEntity<>("Join method executed.", HttpStatus.OK);
    }

    /**
     * Other feature: Reset game result
     */
    @PostMapping("/reset")
    public ResponseEntity<String> reset(@Valid @RequestBody GameRequest gameRequest) {
        /**
         * Given an existing game that have finished,
         * When the reset endpoint is requested,
         * Then the Game's result should be set to zero.
         */

        /**
         * The following logic should be executed in a new service method:
         *
         * 1. Find the existing game based on the UUID.
         * 2. Check if the user belongs to the game.
         * 3. If the user belongs to the game and the game has finished, update the game result to zero.
         * 4. if the user does not belong to the game or if the game has not finished, the app should prevent the update.
         */
        return new ResponseEntity<>("Reset method executed.", HttpStatus.OK);

    }

}
