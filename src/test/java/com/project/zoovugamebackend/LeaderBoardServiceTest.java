package com.project.zoovugamebackend;

import com.project.zoovugamebackend.dto.LeaderBoardResponse;
import com.project.zoovugamebackend.model.Game;
import com.project.zoovugamebackend.model.GameStatus;
import com.project.zoovugamebackend.model.GameTurn;
import com.project.zoovugamebackend.model.User;
import com.project.zoovugamebackend.repository.GameTurnsRepository;
import com.project.zoovugamebackend.service.LeaderBoardService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RunWith(SpringRunner.class)
public class LeaderBoardServiceTest {

    @TestConfiguration
    static class LeaderBoardServiceTestContextConfiguration {
        @Bean
        public LeaderBoardService leaderBoardService() {
            return new LeaderBoardService();
        }
    }

    @Autowired
    private LeaderBoardService leaderBoardService;

    @MockBean
    private GameTurnsRepository gameTurnsRepository;

    @Test
    public void getBestPlayersAllTimeTest() {
        List<GameTurn> allGames = generateGameTurns(15, Instant.now());

        Mockito.when(gameTurnsRepository.findTop10ByGameGameStatusOrderByGameResultInSecAsc(GameStatus.FINISHED))
                .thenReturn(allGames);

        List<LeaderBoardResponse> leaderBoardResponses = leaderBoardService.bestPlayers();

        Assert.assertEquals(allGames.size(), leaderBoardResponses.size());
    }

    @Test
    public void getBestPlayersTodayTest() {
        List<GameTurn> yesterdayGames = generateGameTurns(10, Instant.now().minus(1, ChronoUnit.DAYS));
        List<GameTurn> todayGames = generateGameTurns(5, Instant.now());

        Mockito.when(gameTurnsRepository.findTop10ByGameGameStatusOrderByGameResultInSecAsc(GameStatus.FINISHED))
                .thenReturn(
                        Stream.of(yesterdayGames, todayGames)
                        .flatMap(x -> x.stream())
                        .collect(Collectors.toList())
                );

        List<LeaderBoardResponse> leaderBoardResponses = leaderBoardService.bestPlayersToday();

        Assert.assertEquals(todayGames.size(), leaderBoardResponses.size());
    }
    @Test
    public void getBestPlayersThisYearTest() {
        List<GameTurn> lastYearGames = generateGameTurns(5, ZonedDateTime.now().minusYears(1).toInstant());
        List<GameTurn> thisYearGames = generateGameTurns(2, Instant.now());

        Mockito.when(gameTurnsRepository.findTop10ByGameGameStatusOrderByGameResultInSecAsc(GameStatus.FINISHED))
                .thenReturn(
                        Stream.of(lastYearGames, thisYearGames)
                                .flatMap(x -> x.stream())
                                .collect(Collectors.toList())
                );

        List<LeaderBoardResponse> leaderBoardResponses = leaderBoardService.bestPlayersThisYear();

        Assert.assertEquals(thisYearGames.size(), leaderBoardResponses.size());
    }

    private List<GameTurn> generateGameTurns(Integer numOfGames, Instant createdDate) {
        List<GameTurn> gameTurns = new ArrayList<>();

        for (int i = 1; i <= numOfGames; i++) {
            Game game = new Game();
            game.setUuid(UUID.randomUUID().toString());
            game.setGameStatus(GameStatus.FINISHED);
            game.setResultInSec(60 + i);
            game.setCreatedDate(createdDate);

            User user = new User();
            user.setUsername("waltersteven" + i);
            user.setPassword("123456");
            user.setEmail("wparionasanchez@gmail.com");
            user.setEnabled(true);
            user.setCreated(Instant.now());

            GameTurn gameTurn = new GameTurn();
            gameTurn.setLastPlayedAt(Instant.now());
            gameTurn.setUserTurn(true);
            gameTurn.setGame(game);
            gameTurn.setUser(user);

            gameTurns.add(gameTurn);
        }

        return gameTurns;
    }
}
